//Erik Pope (Agent6)
//9/3/1018
//TicTacToe

#include <Arduboy2.h>
Arduboy2 arduboy;

int8_t selectx = 1;
int8_t selecty = 1;
int8_t board[3][3]={{0,0,0},
                    {0,0,0},
                    {0,0,0}};
int8_t player = 1;
int8_t turn = 1;

const unsigned char select[] PROGMEM  = {
    0x00, 0x00, 0x3c, 0x4, 0x4, 0x4, 0x00, 0x00, 0x00, 0x00, 0x00, 0x4, 0x4, 0x4, 0x3c, 0x00, 0x00, 0x00, 0x78, 0x40, 0x40, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x40, 0x40, 0x78, 0x00,  
};

const unsigned char x[] PROGMEM  = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x30, 0xf0, 0x80, 0x80, 0xd0, 0x70, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x38, 0x2c, 0x6, 0x3, 0xf, 0x38, 0x30, 0x20, 0x00, 0x00, 0x00,  
};

const unsigned char o[] PROGMEM  = {
    0x00, 0x00, 0x00, 0x00, 0xc0, 0xe0, 0x20, 0x70, 0x50, 0x10, 0x30, 0xe0, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf, 0x1f, 0x38, 0x30, 0x20, 0x20, 0x30, 0x18, 0xf, 0x00, 0x00, 0x00, 
};

void setup() {
    arduboy.begin();
    arduboy.clear();
    arduboy.setFrameRate(60);
}

void tie() {
  arduboy.setCursor(48, 57);
  arduboy.print(F("Tie!!!"));
  reset();
}

void win() {
  arduboy.setCursor(36, 57);
  arduboy.print(F("You Win!!!"));
  reset();
}

//Draw game board
void drawBoard(){
    arduboy.setCursor(42, 0);
    arduboy.print(F("Player "));
    arduboy.print(player);
    arduboy.fillRect(56, 8, 1, 48, WHITE);
    arduboy.fillRect(72, 8, 1, 48, WHITE);
    arduboy.fillRect(40, 24, 48, 1, WHITE);
    arduboy.fillRect(40, 40, 48, 1, WHITE);
    //This is the Game cursor
    arduboy.drawBitmap(selectx*16 + 40, selecty*16 + 8, select, 16, 16, WHITE);
    arduboy.display();
}

void drawMoves(){
  for(uint8_t ax=0; ax < 3; ax = ax + 1){
      for(uint8_t ay=0; ay < 3; ay = ay + 1){
        if(board[ax][ay] == 1){
          arduboy.drawBitmap(ax*16 + 40, ay*16 + 8, x, 16, 16, WHITE);
        }else if(board[ax][ay] == 2){
          arduboy.drawBitmap(ax*16 + 40, ay*16 + 8, o, 16, 16, WHITE);
        }
      }
    }
}


void resetBoard(){
  for(uint8_t ax=0; ax < 3; ax = ax + 1){
      for(uint8_t ay=0; ay < 3; ay = ay + 1){
        board[ax][ay]=0;       
      }
  }  
}

//Set all values back to default
void reset() {
    drawBoard();
    delay(2500);
    selectx = 1;
    selecty = 1;
    player = 1;
    turn = 1;
    resetBoard();
}

void gameInput(){
  if(arduboy.justPressed(LEFT_BUTTON)) {
      if(selectx > 0) {
        selectx -= 1;
      }else{
        selectx = 2;
      }
    }
    if(arduboy.justPressed(RIGHT_BUTTON)) {
      if(selectx < 2) {
        selectx += 1;
      }else{
        selectx = 0;
      }
    }
    if(arduboy.justPressed(UP_BUTTON)) {
      if(selecty > 0) {
        selecty -= 1;
      }else{
        selecty = 2;
      }
    }
    if(arduboy.justPressed(DOWN_BUTTON)) {
      if(selecty < 2) {
        selecty += 1;
      }else{
        selecty = 0;
      }
    }
    
//Place X or O if Blank
    if(arduboy.justPressed(A_BUTTON)) {
        if(board[selectx][selecty] == 0){
          board[selectx][selecty] = player;
          turn = turn + 1;
          if(player == 1){
            player=2;
          }else{
            player=1;
          }
        }
    }
    
//Not used    
    if(arduboy.justPressed(B_BUTTON)) {   
    }
}

void gameLogic(){
  // check rows
    for (uint8_t i = 0; i < 3; ++i)
      if(board[0][i]!=0 && board[0][i]==board[1][i] && board[0][i]==board[2][i])
        win();
// check columns
    for (uint8_t i = 0; i < 3; ++i)
      if(board[i][0]!=0 && board[i][0]==board[i][1] && board[i][0]==board[i][2])
        win();
// check diagonal 1
    if(board[0][0]!=0 && board[0][0]==board[1][1] && board[0][0]==board[2][2])
      win();
// check diagonal 2
    if(board[2][0]!=0 && board[2][0]==board[1][1] && board[2][0]==board[0][2])
      win();
// Go to Tie function after 9th turn
    if(turn>9){;
      tie();
    }
}

void loop() {
  if(!arduboy.nextFrame()) {
  return;
  }
    arduboy.pollButtons();
    arduboy.clear();
    
    gameInput();  
    drawMoves();
    gameLogic();   
    drawBoard();
}
